;;; init.el --- Where all the magic begins
;;
;; Part of the Emacs Starter Kit
;;
;; This is the first thing to get loaded.
;;

;; remember this directory
(setq starter-kit-dir
      (file-name-directory (or load-file-name (buffer-file-name))))

;; load up the starter kit
(org-babel-load-file (expand-file-name "starter-kit.org" starter-kit-dir))

;; Start emacs server
(server-start)

;; Tramp to edit as different user
(require 'tramp)
(add-to-list 'backup-directory-alist
             (cons tramp-file-name-regexp nil))


;; Mutt support.                                                                                                                                                          
(setq auto-mode-alist
      (append
       '(("/tmp/mutt.*" . mail-mode)
         )
       auto-mode-alist)
      )




;; No need errors when no errors
(defun demolish-tex-help ()
  (interactive)
  (if (get-buffer "*TeX Help*") ;; Tests if the buffer exists
      (progn ;; Do the following commands in sequence
        (if (get-buffer-window (get-buffer "*TeX Help*")) ;; Tests if the window exists
            (delete-window (get-buffer-window (get-buffer "*TeX Help*")))
          ) ;; That should close the window
        (kill-buffer "*TeX Help*") ;; This should kill the buffer
        )
    )
  )

;; Okular is installed here...
(defun Okular-make-url () (concat
                           "file://"
                           (expand-file-name (funcall file (TeX-output-extension) t)
                                             (file-name-directory (TeX-master-file)))
                           "#src:"
                           (TeX-current-line)
                           (expand-file-name (TeX-master-directory))
                           "./"
                           (TeX-current-file-name-master-relative)))
(add-hook 'LaTeX-mode-hook '(lambda ()
                              (add-to-list 'TeX-expand-list
                                           '("%u" Okular-make-url))))

;; Don't know why, but it works!
(setq TeX-view-program-list
      '(("Okular" "okular --unique %o#src:%n`pwd`/%b")))
(setq TeX-view-program-selection '((output-pdf "Okular") (output-dvi "Okular")))


(defun run-latexmk ()
  (interactive)
  (let ((TeX-save-query nil)
        (TeX-process-asynchronous nil)
        (master-file (TeX-master-file)))
    (TeX-save-document "")
    (TeX-run-TeX "latexmk"
                 (TeX-command-expand "latexmk -pdflatex='pdflatex -file-line-error -synctex=1' -pdf %s" 'TeX-master-file)
                 master-file)
    (if (plist-get TeX-error-report-switches (intern master-file))
        (TeX-next-error t)
      (progn
        (demolish-tex-help)
        (minibuffer-message "latexmk done")))))

;; Add to command list
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (add-to-list 'TeX-command-list
                         '("latexmk" "(run-latexmk)"
                           TeX-run-function nil t :help "Run latexmk") t)
            (setq TeX-command-default "latexmk")))

(add-hook 'LaTeX-mode-hook
          (lambda () (local-set-key (kbd "<f8>") 'run-latexmk)))

;; TeX
(setq TeX-PDF-mode t)
;; Some AucTeX
(setq TeX-parse-self t) ; Enable parse on load.
(setq TeX-auto-save t) ; Enable parse on save.
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (TeX-fold-mode 1))) ; Fold!

;; RefTeX
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

;; SyncTeX
;; forward search
'(TeX-source-correlate-method (quote synctex))
'(TeX-source-correlate-mode t)
'(TeX-source-correlate-start-server t)



;;; init.el ends here
